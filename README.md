# Strength Training Workout Planner

A console Java-program for printing out a workout plan for strength training purposes.

## Features

* ready list of lifts and movements to quickly add
* working sets, repetitions and weights input
* remove exercise from the list
* print to workout routine to screen